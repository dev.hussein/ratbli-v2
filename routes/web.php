<?php

/* 
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

// Switch between the included languages
Route::get('lang/{lang}', 'LanguageController@swap');

Auth::routes();


Route::get('/', function () {
	return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('dashboard');
Route::get('/register/confirm', 'Auth\RegisterConfirmationController@index')->name('register.confirm');


/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

Route::prefix('admin')->group(function() {
  Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
  Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
  Route::get('/', 'DashboardController@index')->name('admin.dashboard');
  Route::post('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

  // Password reset routes
  Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
  Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
  Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
  Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');

  //Admins
  Route::resource('admins', 'AdminController');
  Route::get('profile', 'AdminController@profile')->name('admin.profile');
  Route::patch('profile/{admin}', 'AdminController@updateProfile')->name('admin.update-profile');

  Route::resource('permissions', 'PermissionsController');

  Route::resource('languages', 'LanguageController');

  Route::resource('services', 'ServiceController');

  Route::resource('categories', 'CategoryController');

  Route::resource('users', 'UserController');
  
  //Commands
  Route::get('commands', 'CommandsController@index')->name('commands.index');
  Route::post('run-migration', 'CommandsController@runMigration')->name('run-migration');
  Route::post('install-database', 'CommandsController@installDatabase')->name('install-database');
  Route::post('install-passport', 'CommandsController@installPassport')->name('install-passport');
  Route::post('optimize', 'CommandsController@optimize')->name('optimize');
  Route::post('clear-cache', 'CommandsController@clearCache')->name('clear-cache');
  Route::post('table-truncate/{table}', 'CommandsController@tableTruncate')->name('table-truncate');
  Route::post('table-delete/{table}', 'CommandsController@tableDelete')->name('table-delete');
  Route::get('table-show/{table}', 'CommandsController@tableShow')->name('table-show');
  
});


Route::get('test', function () {

  $service = App\Service::first();

  echo $service->name;


});
