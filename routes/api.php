<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'v1', 'middleware' => 'auth:admin-api'], function () {	
	Route::post('sidebar-toggle', 'API\AdminController@postSidebarToggle');
	Route::post('image-upload', 'API\AdminController@postUploadAvatar');
});


Route::get('languages', 'API\LanguageController@index');
Route::get('services/{locale}', 'API\ServiceController@index');
Route::get('services/{locale}/{id}/categories', 'API\ServiceController@getCategories');
Route::get('categories/{locale}', 'API\CategoryController@index');

//Authentication
Route::post('register/{locale}', 'API\AuthController@register');
Route::post('login/{locale}', 'API\AuthController@login');
Route::post('confirm/{locale}/{mobile}', 'API\AuthController@confirm');
Route::get('logout', 'API\AuthController@logout')->middleware('auth:api');

Route::get('profile', 'API\ProfileController@show');
Route::put('profile/update', 'API\ProfileController@update');