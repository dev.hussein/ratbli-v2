<?php

// Dashboard
Breadcrumbs::register('admin.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push(__('admin.home'), route('admin.dashboard'));
});

//Admins
Breadcrumbs::register('admins.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('admins.admins'), route('admins.index'));
});

Breadcrumbs::register('admins.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admins.index');
    $breadcrumbs->push(__('admins.create_new_admin'), route('admins.create'));
});

Breadcrumbs::register('admins.edit', function ($breadcrumbs, $admin) {
    $breadcrumbs->parent('admins.index');
    $breadcrumbs->push(__('admins.edit_admin'), route('admins.edit', $admin->id));
});

Breadcrumbs::register('admin.profile', function ($breadcrumbs) {
    $breadcrumbs->parent('admins.index');
    $breadcrumbs->push('الصفحة الشخصية', route('admin.profile'));
});

//Users
Breadcrumbs::register('users.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('users.users'), route('users.index'));
});

Breadcrumbs::register('users.create', function ($breadcrumbs) {
    $breadcrumbs->parent('users.index');
    $breadcrumbs->push(__('users.create_new_user'), route('users.create'));
});

Breadcrumbs::register('users.edit', function ($breadcrumbs, $admin) {
    $breadcrumbs->parent('users.index');
    $breadcrumbs->push(__('users.edit_user'), route('users.edit', $admin->id));
});

//Permissions
Breadcrumbs::register('permissions.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('permissions.permissions'), route('permissions.index'));
});

Breadcrumbs::register('permissions.create', function ($breadcrumbs) {
    $breadcrumbs->parent('permissions.index');
    $breadcrumbs->push(__('permissions.create_new_permission'), route('permissions.create'));
});

Breadcrumbs::register('permissions.edit', function ($breadcrumbs, $permission) {
    $breadcrumbs->parent('permissions.index');
    $breadcrumbs->push(__('permissions.edit_permission'), route('permissions.edit', $permission->id));
});

//Languages
Breadcrumbs::register('languages.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('languages.languages'), route('languages.index'));
});

Breadcrumbs::register('languages.create', function ($breadcrumbs) {
    $breadcrumbs->parent('languages.index');
    $breadcrumbs->push(__('languages.create_new_language'), route('languages.create'));
});

Breadcrumbs::register('languages.edit', function ($breadcrumbs, $language) {
    $breadcrumbs->parent('languages.index');
    $breadcrumbs->push(__('languages.edit_language'), route('languages.edit', $language->id));
});

//Services
Breadcrumbs::register('services.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('services.services'), route('services.index'));
});

Breadcrumbs::register('services.create', function ($breadcrumbs) {
    $breadcrumbs->parent('services.index');
    $breadcrumbs->push(__('services.create_new_service'), route('services.create'));
});

Breadcrumbs::register('services.edit', function ($breadcrumbs, $service) {
    $breadcrumbs->parent('services.index');
    $breadcrumbs->push(__('services.edit_service'), route('services.edit', $service->id));
});

//Categories
Breadcrumbs::register('categories.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('categories.categories'), route('categories.index'));
});

Breadcrumbs::register('categories.create', function ($breadcrumbs) {
    $breadcrumbs->parent('categories.index');
    $breadcrumbs->push(__('categories.create_new_category'), route('categories.create'));
});

Breadcrumbs::register('categories.edit', function ($breadcrumbs, $admin) {
    $breadcrumbs->parent('categories.index');
    $breadcrumbs->push(__('categories.edit_category'), route('categories.edit', $admin->id));
});
