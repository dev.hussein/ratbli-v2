<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>لوحة المعلومات | تسجيل الدخول</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="{!! asset('assets/dashboard/css/bootstrap-rtl.css') !!}">
  <link rel="stylesheet" href="{!! asset('assets/dashboard/bower_components/Ionicons/css/ionicons.min.css') !!}">
  <link rel="stylesheet" href="{!! asset('assets/dashboard/css/AdminLTE.min.css') !!}">
  <link rel="stylesheet" href="{!! asset('assets/dashboard/css/rtl.css') !!}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{!! asset('assets/dashboard/plugins/iCheck/square/blue.css') !!}">

  <style>
    .login-page {
      height: 100vh;
      width: 100vh;
      background-color: #ddd;
      background: url(https://source.unsplash.com/category/nature/1920x1080) no-repeat center center fixed;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
    }
    .login-box {
        background-color: rgba(0, 0, 0, 0.7);
        border: 1px solid rgba(255, 255, 255, 0.2);
        border-radius: 3px !important;
        padding: 20px;
    }
    .overlay-body {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.6);
    }
    .fc-outline-dark,
    .fc-outline-dark:focus,
    .fc-outline-dark:active {
      background-color: transparent;
    }
    .fc-outline-dark {
      border-color: rgba(255, 255, 255, 0.3);
      color: #fff;
    }
    .fc-outline-dark:focus, .fc-outline-dark:active {
      border-color: rgba(255, 255, 255, 0.75);
      color: rgba(255, 255, 255, 0.75);
    }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="overlay-body">
  
<div class="login-box">
  <div class="login-logo">
    <a href="{{ route('admin.dashboard') }}"><span style="color:#99ce66">لوحة</span>&nbsp;<span style="color:#c4c4c4">المعلومات</span></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg" style="color:#c4c4c4">تسجيل الدخول</p>

    {!! Form::open(['route' => 'admin.login.submit']) !!} 
      <div class="form-group has-feedback">
        {!! Form::text('email', null, ['class' => 'form-control fc-outline-dark', 'id' => 'email', 'placeholder' => 'اسم المستخدم']) !!}
        <span class="icon ion-person form-control-feedback"></span>
      </div>

      <div class="form-group has-feedback">
        {!! Form::password('password', ['class' => 'form-control fc-outline-dark', 'id' => 'password', 'placeholder' => 'كلمة المرور']) !!}
        <span class="icon ion-locked form-control-feedback"></span>
      </div>

      <div class="row">
        <div class="col-xs-12">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" {{ old('remember') ? 'checked' : '' }}> تذكرنى
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-12">
          <button type="submit" class="btn btn-info btn-block btn-flat">دخول</button>
        </div>
        <!-- /.col -->
      </div>
    {!! Form::close() !!}

    <div class="text-center" style="margin-top: 15px">
      <a href="{{ route('admin.password.request') }}">نسيت كلمة المرور</a>
    </div>
  </div>
  <!-- /.login-box-body -->
</div>
</div>
<!-- /.login-box -->

<script src="{!! asset('assets/dashboard/bower_components/jquery/dist/jquery.min.js') !!}"></script>
<script src="{!! asset('assets/dashboard/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}"></script>
<script src="{!! asset('assets/dashboard/plugins/iCheck/icheck.min.js') !!}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
