@extends('layouts.app')

@section('meta_description', 'Login Page')
@section('title', 'Login')

@section('content')
<div class="container">
    <div class="columns">
        <div class="column is-three-fifths is-offset-one-fifth">
            <div class="box">
                <h2 class="title">Login</h2>

                @include('flash::message')

                <form method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="field">
                        <div class="control has-icons-left">
                            <input class="input {{ $errors->has('email') ? ' is-danger' : '' }}" type="email" name="email" placeholder="Email" value="{{ old('email') }}" required>
                            <span class="icon is-small is-left">
                                <i class="fa fa-envelope"></i>
                            </span>
                        </div>
                        @if ($errors->has('email'))
                            <p class="help is-danger">
                                {{ $errors->first('email') }}
                            </p>
                        @endif
                    </div>
                    <div class="field">
                        <div class="control has-icons-left">
                            <input class="input {{ $errors->has('password') ? ' is-danger' : '' }}" type="password" name="password" placeholder="Password" required>
                            <span class="icon is-small is-left">
                                <i class="fa fa-lock"></i>
                            </span>
                        </div>
                        @if ($errors->has('password'))
                            <p class="help is-danger">
                                {{ $errors->first('password') }}
                            </p>
                        @endif
                    </div>
                    <div class="field">
                        <div class="control">
                            <label class="checkbox">
                                <input type="checkbox" {{ old('remember') ? 'checked' : '' }}>
                                Remember me
                            </label>
                        </div>
                    </div>
                    <div class="field">
                        <div class="control">
                            <a href="{{ route('password.request') }}">
                                Forgot Your Password?
                            </a>
                        </div>
                    </div>
                    
                    <div class="field is-grouped">
                      <div class="control">
                        <a class="button is-primary" href="{{ route('register') }}">
                            Register
                        </a>
                      </div>
                      <div class="control">
                        <button class="button is-primary">
                            Login
                        </button>
                      </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
