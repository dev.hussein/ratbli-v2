@extends('layouts.app')

@section('content')
<div class="container">
    <div class="columns">
        <div class="column is-12">
            <div class="box">
                <h2 class="title">Dashboard</h2>

                <div>
                    {{--<img src="{{ getThumbnail("ccc.jpg", 150, 150, "background") }}">
                    <img src="{{ getThumbnail("ccc.jpg", 250, 250, "background") }}">
                    <img src="{{ getThumbnail("ccc.jpg", 400, 400, "background") }}">--}}
                </div>
                <p>{{ \Date::now()->format('l j F Y H:i:s')  }}</p>
            </div>
        </div>
    </div>
</div>
@endsection
