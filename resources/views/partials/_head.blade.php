<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="@yield('meta_description', 'Website default description')">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<script>
	window.App = {!! json_encode([
	    'csrfToken' => csrf_token(),
	    'baseUrl'   => url("/") 
	]) !!};
</script>

<title>{{ config('app.name', 'Master') }} | @yield('title', 'Home')</title>

<!-- Fonts -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.css') }}">

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">

@langRTL
<link href="{{ asset('css/rtl.css') }}" rel="stylesheet">
@endif

@yield('styles')