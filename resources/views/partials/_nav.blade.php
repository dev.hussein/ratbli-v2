<nav class="navbar is-info">
    <div class="navbar-brand">
        <a class="navbar-item" href="{{ url('/') }}">
            <img src="{{ asset('images/bulma-logo-white.png') }}" alt="File Hosting" width="112" height="28">
        </a>
    </div>
    <div class="navbar-menu">
        <div class="navbar-start">
            <a class="navbar-item{{ active('home', ' is-active') }}" href="{{ url('/home') }}">
                Home
            </a>
        </div>
        <div class="navbar-end">
            <div class="navbar-item has-dropdown is-hoverable">
                <a class="navbar-link">
                    {{ App\Language::where('code', App::getLocale())->value('name') }}
                </a>

                <div class="navbar-dropdown is-right">
                    @foreach (App\Language::all() as $lang)  
                        @if ($lang->code != App::getLocale()) 
                            <a class="navbar-item" href="{{ url('lang', $lang->code) }}">{{ $lang->name }}</a>
                        @endif
                    @endforeach
                </div>
            </div>

            @guest
                <a class="navbar-item{{ active('login', ' is-active') }}" href="{{ route('login') }}">Login</a>
                <a class="navbar-item{{ active('register', ' is-active') }}" href="{{ route('register') }}">Register</a>
            @else
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link">
                        <span class="fa fa-user-o"></span> &nbsp;
                        {{ Auth::user()->name }}
                    </a>

                    <div class="navbar-dropdown is-right">
                            <a class="navbar-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                    </div>
                </div>
            @endguest
        </div>
    </div>
</nav>