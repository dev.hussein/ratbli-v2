@extends('layouts.app')

@section('content')
<div class="container">
    <div class="columns">
        <div class="column is-12">
            <div class="box">
                <h2 class="title">Dashboard</h2>

                @if (session('status'))
                    <article class="message is-success">
                        <div class="message-body">
                            {{ session('status') }}
                        </div>
                    </article>
                @endif

                You are logged in!
            </div>
        </div>
    </div>
</div>
@endsection
