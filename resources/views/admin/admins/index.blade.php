@extends('admin.layouts.app')

@section('title', __('admins.admins'))

@section('styles')
  <link href="{{ asset('assets/dashboard/bower_components/datatables/css/dataTables.bootstrap.min.css') }}" rel="stylesheet"> 
@stop

@section('pageheader')
  <h1>
    {{ __('admins.admins') }}
  <small>{{ __('admin.show_all') }}</small>
  </h1>
@stop

@section('content')
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">{{ __('admin.show_all') }}</h3>

      <div class="box-tools">
      <a href="{{ route('admins.create') }}" class="btn btn-default btn-flat"><i class="icon ion-ios-plus"></i>&nbsp;&nbsp;{{ __('admin.create_new') }}</a>

      <button class="btn btn-default btn-flat" onclick="reload_table()"><i class="ion-ios-refresh"></i>&nbsp;&nbsp;{{ __('admin.refresh') }}</button>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="dt" class="table table-bordered table-striped">
        <thead>
        <tr>
        <th>{{ __('admins.name') }}</th>
        <th>{{ __('admins.username') }}</th>
        <th>{{ __('admins.email') }}</th>
        <th>{{ __('admin.status') }}</th>
        <th>{{ __('admin.control') }}</th>
        </tr>
        </thead>
        <tbody></tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
@stop

@section('scripts')
  <script src="{{ asset('assets/dashboard/bower_components/dataTables/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/dashboard/bower_components/dataTables/js/dataTables.bootstrap.min.js') }}"></script>
  <script>
    let url = '{!! route('admins.index') !!}';
  </script>

  <script type="text/javascript" src="{{ asset('assets/dashboard/js/admins.js') }}"></script>
@stop