@extends('admin.layouts.app')

@section('title', 'الصفحة الشخصية')

@section('pageheader')
  <h1>
    المشرفين
    <small>الصفحة الشخصية</small>
  </h1>
@stop

@section('content')
  <div class="row">
  {!! Form::model($admin , ['method' => 'PATCH', 'route' => ['admin.update-profile' , $admin->id]]) !!}
    <!-- left column -->
    <div class="col-md-7">

      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">الصفحة الشخصية</h3>
        </div>

          <div class="box-body">
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              <label for="name">الاسم</label>
              {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'الاسم']) !!}

              @if ($errors->has('name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
            </div>

            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
              <label for="username">اسم المستخدم</label>
              {!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'اسم المستخدم', 'readonly' => 'readonly']) !!}

              @if ($errors->has('username'))
                  <span class="help-block">
                      <strong>{{ $errors->first('username') }}</strong>
                  </span>
              @endif
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email">البريد الالكترونى</label>
              {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'البريد الالكترونى', 'readonly' => 'readonly']) !!}

              @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
            </div>

            <div class="form-group">
              <label for="password">تغيير كلمة المرور</label>
              <div class="input-group">
                <span class="input-group-addon">
                  {{ Form::checkbox('password_change') }}<span></span>
                </span>
                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'كلمة المرور الجديدة']) !!}
              </div>
            </div>


            <div class="form-group{{ $errors->has('active') ? ' has-error' : '' }}">
              <label for="active">الحالة</label>
              {{ Form::select('active', ['1' => 'مفعل', '0' => 'معطل'], null, ['class' => 'form-control selectpicker']) }}

              @if ($errors->has('active'))
                  <span class="help-block">
                      <strong>{{ $errors->first('active') }}</strong>
                  </span>
              @endif
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <div class="col-md-6">
              <button class="btn btn-info btn-flat btn-block m-b-10"><i class="icon ion-ios-plus-outline"></i>&nbsp;حفظ</button>
            </div>
          </div>
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
  {!! Form::close() !!}
    <!-- right column -->
    <div class="col-md-5 hide">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">الصورة الشخصية</h3>
        </div>
  
        <div class="box-body">
          <image-upload img-url="{{ $admin->avatar_path() }}" id = "{{ $admin->id }}"></image-upload>
        </div>

      </div>
      <!-- /.box -->
    </div>
    <!--/.col (right) -->
  </div>
@stop