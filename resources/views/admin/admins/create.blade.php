@extends('admin.layouts.app')

@section('title', __('admins.create_new_admin'))

@section('pageheader')
  <h1>
    {{ __('admins.admins') }}
    <small>{{ __('admins.create_new_admin') }}</small>
  </h1>
@stop

@section('content')
  <div class="row">
  {!! Form::open(['route' => 'admins.store']) !!}
    <!-- left column -->
    <div class="col-md-7">

      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">{{ __('admins.create_new_admin') }}</h3>
        </div>

          <div class="box-body">
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              <label for="name">{{ __('admins.name') }}</label>
              {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => __('admins.name')]) !!}

              @if ($errors->has('name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
            </div>

            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
              <label for="username">{{ __('admins.username') }}</label>
              {!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => __('admins.username')]) !!}

              @if ($errors->has('username'))
                  <span class="help-block">
                      <strong>{{ $errors->first('username') }}</strong>
                  </span>
              @endif
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email">{{ __('admins.email') }}</label>
              {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => __('admins.email')]) !!}

              @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password">{{ __('admins.password') }}</label>
              {!! Form::password('password', ['class' => 'form-control', 'placeholder' => __('admins.password')]) !!}

              @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
            </div>


            <div class="form-group">
            <label for="active">{{ __('admin.status') }}</label>
              {{ Form::select('active', ['1' => __('admin.active'), '0' => __('admin.disabled')], 1, ['class' => 'form-control selectpicker']) }}
            </div>

            <hr>

            <div class="form-group">
            <label for="permission_id">{{ __('admins.permissions') }}</label>
                @foreach($permissions as $permission)
                  <div class="checkbox">
                      <label for="permission_id{{ $permission->id }}">
                        <input type="checkbox" name="permission_id[]" id="permission_id{{ $permission->id }}" value="{{ $permission->id }}" {{ ( is_array(old('permission_id')) && in_array($permission->id, old('permission_id')) ) ? 'checked ' : '' }}>

                      {{ $permission->name }}
                    </label>
                  </div>
                @endforeach
            </div>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <div class="col-md-6">
              <button class="btn btn-info btn-flat btn-block"><i class="icon ion-ios-plus-outline"></i>&nbsp;{{ __('admin.save') }}</button>
            </div>
            <div class="col-md-6">
              <a href="{{ route('admins.index') }}" class="btn btn-default btn-flat btn-block"><i class="icon ion-log-out"></i>&nbsp;{{ __('admin.back') }}</a>
            </div>
          </div>
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
  {!! Form::close() !!}
  </div>
@stop