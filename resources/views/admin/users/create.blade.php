@extends('admin.layouts.app')

@section('title', __('users.create_new_user'))

@section('pageheader')
  <h1>
    {{ __('users.users') }}
  <small>{{ __('users.create_new_user') }}</small>
  </h1>
@stop

@section('content')
  <div class="row">
  {!! Form::open(['route' => 'users.store']) !!}
    <!-- left column -->
    <div class="col-md-7">

      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">{{ __('users.create_new_user') }}</h3>
        </div>

          <div class="box-body">
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              <label for="name">{{ __('users.name') }}</label>
              {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => __('users.name')]) !!}

              @if ($errors->has('name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
            </div>

            <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
            <label for="mobile">{{ __('users.mobile') }}</label>
              {!! Form::text('mobile', null, ['class' => 'form-control', 'placeholder' => __('users.mobile')]) !!}

              @if ($errors->has('mobile'))
                  <span class="help-block">
                      <strong>{{ $errors->first('mobile') }}</strong>
                  </span>
              @endif
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email">{{ __('users.email') }}</label>
              {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => __('users.email')]) !!}

              @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password">{{ __('users.password') }}</label>
              {!! Form::password('password', ['class' => 'form-control', 'placeholder' => __('users.password')]) !!}

              @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
            </div>


            <div class="form-group">
            <label for="status">{{ __('admin.status') }}</label>
              {{ Form::select('status', ['1' => __('admin.active'), '0' => __('admin.disabled')], 1, ['class' => 'form-control selectpicker']) }}
            </div>


          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <div class="col-md-6">
            <button class="btn btn-info btn-flat btn-block"><i class="icon ion-ios-plus-outline"></i>&nbsp;{{ __('admin.save') }}</button>
            </div>
            <div class="col-md-6">
            <a href="{{ route('users.index') }}" class="btn btn-default btn-flat btn-block"><i class="icon ion-log-out"></i>&nbsp;{{ __('admin.back') }}</a>
            </div>
          </div>
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
  {!! Form::close() !!}
  </div>
@stop