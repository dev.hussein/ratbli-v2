@foreach($childs as $child)

<tr class="treegrid-{{$child->id}} treegrid-parent-{{$child->parent_id}}">
  <td>{!! $child->name !!}</td>
  <td>
    <img src="{{ $child->photo_path() }}" class="img img-responsive" width="75px">
  </td>
  <td>
    {!! $child->check_status() !!}
  </td>
  <td>
    <a href="{{ route('categories.edit', $child) }}" class="btn btn-info btn-sm btn-flat"><i class="icon ion-ios-compose-outline"></i></a>
  </td>
</tr>

@if(count($child->childs))
    @include('admin.categories.childs', ['childs' => $child->childs])
@endif

@endforeach