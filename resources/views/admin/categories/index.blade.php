@extends('admin.layouts.app')

@section('title', __('categories.categories'))

@section('styles')
  <link href="{{ asset('assets/dashboard/bower_components/jquery-treegrid/css/jquery.treegrid.css') }}" rel="stylesheet"> 
@stop

@section('pageheader')
  <h1>
    {{ __('categories.categories') }}
  <small>{{ __('admin.show_all') }}</small>
  </h1>
@stop

@section('content')
  <div class="box">
    <div class="box-header with-border">
    <h3 class="box-title">{{ __('admin.show_all') }}</h3>

      <div class="box-tools">
      <a href="{{ route('categories.create') }}" class="btn btn-default btn-flat"><i class="icon ion-ios-plus"></i>&nbsp;&nbsp;{{ __('admin.create_new') }}</a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table class="table table-bordered table-striped tree">
        <thead>
        <tr>
        <th>{{ __('categories.name') }}</th>
        <th>{{ __('categories.image') }}</th>
        <th>{{ __('admin.status') }}</th>
        <th>{{ __('admin.control') }}</th>
        </tr>
        </thead>
        <tbody>
          @unless ($categories->count())
            <tr>
            <td colspan="3">{{ __('admin.no_data') }}</td>
            </tr>
          @else
          @foreach($categories as $category)
            <tr class="treegrid-{{$category->id}}">
              <td>{!! $category->name !!}</td>
              <td>
                <img src="{{ $category->photo_path() }}" class="img img-responsive" width="75px">
              </td>
              <td>
                {!! $category->check_status() !!}
              </td>
              <td>
                <a href="{{ route('categories.edit', $category) }}" class="btn btn-info btn-sm btn-flat"><i class="icon ion-ios-compose-outline"></i></a>
              </td>
            </tr>

            @if(count($category->childs))
                @include('admin.categories.childs', ['childs' => $category->childs])
            @endif

          @endforeach
          @endunless
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
@stop

@section('scripts')
  <script src="{{ asset('assets/dashboard/bower_components/jquery-treegrid/js/jquery.treegrid.js') }}"></script>
  <script src="{{ asset('assets/dashboard/bower_components/jquery-treegrid/js/jquery.treegrid.bootstrap3.js') }}"></script>
  <script>
    $(document).ready(function() {
        $('.tree').treegrid({
            expanderExpandedClass: 'ion-ios-minus-outline',
            expanderCollapsedClass: 'ion-ios-plus'
        });
    });
</script>
@stop