<!DOCTYPE html>
<html>
<head>
  @include('admin.partials._head')
</head>
<body class="hold-transition skin-black sidebar-mini
            {{ (auth()->user('admin')->options['sidebar']) ? 'sidebar-collapse' : '' }}">

<div class="wrapper" id="app">

  @include('admin.partials._header')

  @include('admin.partials._sidebar')

  <div class="content-wrapper">

    <section class="content-header">

      @yield('pageheader')

      {{ Breadcrumbs::render() }}

    </section>

    <section class="content"> 
      @include('admin.errors.list')

      @yield('content')

    </section>

  </div>

  @include('admin.partials._footer')

  @include('admin.partials.control-sidebar')

</div>

  @include('admin.partials._javascript')

</body>
</html>

