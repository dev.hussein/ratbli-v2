<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
    <li class="header">{{ __('admin.menu') }}</li>
      
    <li class="{{ active(['admin.dashboard']) }}"><a href="{{ route('admin.dashboard') }}"><i class="fa fa-home"></i> <span>{{ __('admin_sidebar.home') }}</span></a></li>


      <li class="{{ active('admins.*') }}"><a href="{{ route('admins.index') }}"><i class="fa fa-users"></i> <span>{{ __('admin_sidebar.admins') }}</span></a></li>

      <li class="{{ active('users.*') }}"><a href="{{ route('users.index') }}"><i class="fa fa-users"></i> <span>{{ __('admin_sidebar.users') }}</span></a></li>

      <li class="{{ active('permissions.*') }}"><a href="{{ route('permissions.index') }}"><i class="fa fa-lock"></i> <span>{{ __('admin_sidebar.permissions') }}</span></a></li>

      <li class="{{ active('languages.*') }}"><a href="{{ route('languages.index') }}"><i class="fa fa-globe"></i> <span>{{ __('admin_sidebar.languages') }}</span></a></li>

      <li class="{{ active('services.*') }}"><a href="{{ route('services.index') }}"><i class="fa fa-globe"></i> <span>{{ __('admin_sidebar.services') }}</span></a></li>

      <li class="{{ active('categories.*') }}"><a href="{{ route('categories.index') }}"><i class="fa fa-globe"></i> <span>{{ __('admin_sidebar.categories') }}</span></a></li>


      {{--<li class="{{ active('commands.index') }}"><a href="{{ route('commands.index') }}"><i class="fa fa-terminal"></i> <span>الأوامر</span></a></li>--}}
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>