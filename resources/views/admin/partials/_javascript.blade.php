<script src="{!! asset('assets/dashboard/bower_components/jquery/dist/jquery.min.js') !!}"></script>
<script src="{!! asset('assets/dashboard/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}"></script>
<script src="{{ asset('assets/dashboard/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/bower_components/bootstrap-select/dist/js/i18n/defaults-ar_AR.js') }}"></script>
<script src="{!! asset('assets/dashboard/bower_components/fastclick/lib/fastclick.min.js') !!}"></script>
<script src="{!! asset('assets/dashboard/js/adminlte.min.js') !!}"></script>
<script src="{!! asset('assets/dashboard/js/admin.js') !!}"></script>
	
<script>
  $(function () {
    $('.select2').select2({
    	dir: 'rtl'
    })
  });
</script>

@yield('scripts')