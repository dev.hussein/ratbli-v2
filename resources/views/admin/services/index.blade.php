@extends('admin.layouts.app')

@section('title', __('services.services'))

@section('pageheader')
  <h1>
    {{ __('services.services') }}
    <small>{{ __('admin.show_all') }}</small>
  </h1>
@stop

@section('content')
  <div class="box">
    <div class="box-header with-border">
    <h3 class="box-title">{{ __('admin.show_all') }}</h3>

      <div class="box-tools">
      <a href="{{ route('services.create') }}" class="btn btn-default btn-flat"><i class="icon ion-ios-plus"></i>&nbsp;&nbsp;{{ __('admin.create_new') }}</a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table class="table table-bordered table-striped">
        <thead>
        <tr>
        <th>{{ __('services.name') }}</th>
        <th>{{ __('services.image') }}</th>
        <th>{{ __('admin.status') }}</th>
        <th>{{ __('admin.control') }}</th>
        </tr>
        </thead>
        <tbody>
          @unless ($services->count())
            <tr>
            <td colspan="4">{{ __('admin.no_data') }}</td>
            </tr>
          @else
          @foreach($services as $service)
            <tr>
              <td>{!! $service->name !!}</td>
              <td>
                <img src="{{ $service->photo_path() }}" class="img img-responsive" width="50px">
              </td>
              <td>
                {!! $service->check_status() !!}
              </td>
              <td>
                <a href="{{ route('services.edit', $service) }}" class="btn btn-info btn-sm btn-flat"><i class="icon ion-ios-compose-outline"></i></a>
              </td>
            </tr>
          @endforeach
          @endunless
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
@stop
