@extends('admin.layouts.app')

@section('title', __('services.create_new_service'))

@section('pageheader')
  <h1>
    {{ __('services.services') }}
  <small>{{ __('services.create_new_service') }}</small>
  </h1>
@stop

@section('content')
  <div class="row">
  {!! Form::open(['route' => 'services.store', 'files' => true]) !!}
    <!-- left column -->
    <div class="col-md-6">
      <div class="box">
        <div class="box-header with-border">
        <h3 class="box-title">{{ __('services.create_new_service') }}</h3>
        </div>

          <div class="box-body">
            <div class="form-group">
            <label for="status">{{ __('admin.status') }}</label>
              {{ Form::select('status', ['1' => __('admin.active'), '0' => __('admin.disabled')], 1, ['class' => 'form-control selectpicker']) }}
            </div>


            <div class="form-group">
            <label for="image">{{ __('services.image') }}</label>
                <file-field></file-field>
            </div>
          </div>

          <div class="box-footer">
            <div class="col-md-6">
            <button class="btn btn-info btn-flat btn-block m-b-10"><i class="icon ion-ios-plus-outline"></i>&nbsp;{{ __('admin.save') }}</button>
            </div>
            <div class="col-md-6">
            <a href="{{ route('services.index') }}" class="btn btn-default btn-flat btn-block"><i class="icon ion-log-out"></i>&nbsp;{{ __('admin.back') }}</a>
            </div>
          </div>
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->

    <div class="col-md-6">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          @foreach(languages() as $lang)
          <li>
            <a href="#{{ $lang->code }}" data-toggle="tab">{{ $lang->name }}</a>
          </li>
          @endforeach
        </ul>
        <div class="tab-content">
          @foreach(languages() as $lang)
          <div class="tab-pane" id="{{ $lang->code }}">
            <div class="form-group">
              <label for="name">{{ __('services.name') }} - {{ $lang->name }}</label>
              {!! Form::text("name_{$lang->code}", null, ['class' => 'form-control', 'placeholder' => __('services.name')." - {$lang->name}"]) !!}
            </div> 
          </div>
          @endforeach
          <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
      </div>
      <!-- nav-tabs-custom -->
    </div>
  {!! Form::close() !!}
  </div>
@stop

@section('scripts')
    <script>
        $(".nav-tabs-custom ul li").first().addClass("active");
        $(".tab-content .tab-pane").first().addClass("active");
    </script>
@endsection
