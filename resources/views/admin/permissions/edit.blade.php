@extends('admin.layouts.app')

@section('title', __('permissions.edit_permission'))

@section('pageheader')
  <h1>
    {{  __('permissions.permissions') }}
  <small>{{  __('permissions.edit_permission') }}</small>
  </h1>
@stop

@section('content')
  <div class="row">
  {!! Form::model($permission , ['method' => 'PATCH', 'route' => ['permissions.update' , $permission], 'files' => true]) !!}
    <!-- left column -->
    <div class="col-md-7">

      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">{{  __('permissions.edit_permission') }}</h3>
        </div>

          <div class="box-body">
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              <label for="name">{{  __('permissions.name') }}</label>
              {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => __('permissions.name')]) !!}

              @if ($errors->has('name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
            </div>

          </div>

          <div class="box-footer">
            <div class="col-md-6">
              <button class="btn btn-info btn-flat btn-block m-b-10"><i class="icon ion-ios-plus-outline"></i>&nbsp;{{  __('admin.save') }}</button>
            </div>
            <div class="col-md-6">
              <a href="{{ route('permissions.index') }}" class="btn btn-default btn-flat btn-block"><i class="icon ion-log-out"></i>&nbsp;{{  __('admin.back') }}</a>
            </div>
          </div>
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
  {!! Form::close() !!}
  </div>
@stop
