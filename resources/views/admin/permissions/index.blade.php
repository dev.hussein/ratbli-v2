@extends('admin.layouts.app')

@section('title', __('permissions.permissions'))

@section('styles')
  <script src="{{ asset('assets/dashboard/bower_components/sweetalert2/sweetalert2.min.js') }}"></script>
  <link href="{{ asset('assets/dashboard/bower_components/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">
@stop

@section('pageheader')
  <h1>
    {{ __('permissions.permissions') }}
  <small>{{ __('admin.show_all') }}</small>
  </h1>
@stop

@section('content')
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">{{ __('admin.show_all') }}</h3>

      <div class="box-tools">
          <a href="{{ route('permissions.create') }}" class="btn btn-default btn-flat"><i class="icon ion-ios-plus"></i>&nbsp;&nbsp;{{ __('admin.create_new') }}</a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>{{ __('permissions.name') }}</th>
          <th>{{ __('admin.control') }}</th>
        </tr>
        </thead>
        <tbody>
          @foreach($permissions as $permission)
            <tr>
              <td>{!! $permission->name !!}</td>
              <td>
                <a href="{{ route('permissions.edit', $permission) }}" class="btn btn-info btn-sm btn-flat"><i class="icon ion-ios-compose-outline"></i></a>
                <a href="javascript:void(0)" data-id="{{ $permission->id }}" onclick="deleteData(this)" class="btn btn-danger btn-sm btn-flat"><i class="icon ion-ios-trash-outline"></i></a>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
@stop

@section('scripts')
  <script>
    function deleteData(identifier){
        var csrf_token = $('meta[name="csrf-token"]').attr('content');

        let el = $(identifier);
        let row = identifier.parentNode.parentNode;
        let id = el.data('id');

       swal({
            title: 'هل أنت متأكد؟',
            text: "",
            type: '',
            showCancelButton: true,
            confirmButtonText: '<i class="icon ion-ios-checkmark-outline"></i>&nbsp;موافق',
            cancelButtonText: '<i class="icon ion-ios-close-outline"></i>&nbsp;إلغاء'
        }).then(function () {
            $.ajax({
                url : "{{ url('admin/permissions') }}" + '/' + id,
                type : "POST",
                data : {'_method' : 'DELETE', '_token' : csrf_token},
                success : function(data) {
                    row.parentNode.removeChild(row);
                    swal({
                        title: '',
                        text: data.message,
                        type: '',
                        timer: '1500',
                        showConfirmButton: false
                    })
                },
                error : function () {
                    swal({
                        title: 'Oops...',
                        text: data.message,
                        type: 'error',
                        timer: '1500'
                    })
                }
            });
        });
      }
</script>
@stop