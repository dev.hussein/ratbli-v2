@extends('admin.layouts.app')

@section('title', 'الأوامر')

@section('pageheader')
  <h1>
    Commands
    <small>show all</small>
  </h1>
@stop

@section('content')
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Commands</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      {!! Form::open(['route' => 'install-database', 'style' => 'display:inline']) !!}
      <button type="submit" class="btn btn-app">
        <i class="fa fa-database"></i> Install Database
      </button>
      {!! Form::close() !!}

      {!! Form::open(['route' => 'install-passport', 'style' => 'display:inline']) !!}
        <button type="submit" class="btn btn-app">
          <i class="fa fa-user-secret"></i> Install Passport
        </button>
      {!! Form::close() !!}

      {!! Form::open(['route' => 'run-migration', 'style' => 'display:inline']) !!}
        <button type="submit" class="btn btn-app">
          <i class="fa fa-play"></i> Run Migration
        </button>
      {!! Form::close() !!}

      {!! Form::open(['route' => 'optimize', 'style' => 'display:inline']) !!}
      <button type="submit" class="btn btn-app">
        <i class="fa fa-refresh fa-spin"></i> Optimize
      </button>
      {!! Form::close() !!}

      {!! Form::open(['route' => 'clear-cache', 'style' => 'display:inline']) !!}
      <button type="submit" class="btn btn-app">
        <i class="fa fa-refresh fa-spin"></i> Clear Cache
      </button>
      {!! Form::close() !!}

    </div>
    <!-- /.box-body -->
  </div>

  <div class="row">
    <div class="col-md-6">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Database Tables</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tbody>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Table</th>
                    <th>Rows</th>
                    <th>Action</th>
                  </tr>

                  @foreach($tables as $key => $value)
                    <tr>
                      <td>{{ $key }}</td>
                      <td>{{ $value }}</td>
                      <td>{{ \DB::table($value)->count() }}</td>
                      <td>
                        <a href="{{ route('table-show', $value) }}" class="btn btn-default btn-sm btn-flat">
                          <i class="icon ion-ios-eye"></i>
                        </a>

                        {!! Form::open(['route' => ['table-truncate', $value], 'style' => 'display:inline']) !!}
                          <button type="submit" class="btn btn-info btn-sm btn-flat">
                            <i class="icon ion-battery-empty"></i>
                          </button>
                        {!! Form::close() !!}

                        {!! Form::open(['route' => ['table-delete', $value], 'style' => 'display:inline']) !!}
                          <button type="submit" class="btn btn-danger btn-sm btn-flat">
                            <i class="icon ion-ios-trash"></i>
                          </button>
                        {!! Form::close() !!}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>

    <div class="col-md-6">
      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">Coming soon...</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <ul>
            <li>Upload Database</li>
            <li>Backup Database</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
@stop
