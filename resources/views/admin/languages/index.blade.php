@extends('admin.layouts.app')

@section('title', __('languages.languages'))

@section('pageheader')
  <h1>
    {{ __('languages.languages') }}
  <small>{{ __('admin.show_all') }}</small>
  </h1>
@stop

@section('content')
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">{{ __('admin.show_all') }}</h3>

      <div class="box-tools">
          <a href="{{ route('languages.create') }}" class="btn btn-default btn-flat"><i class="icon ion-ios-plus"></i>&nbsp;&nbsp;{{ __('admin.create_new') }}</a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>{{ __('languages.name') }}</th>
          <th>{{ __('languages.code') }}</th>
          <th>{{ __('languages.direction') }}</th>
          <th>{{ __('languages.flag') }}</th>
          <th>{{ __('admin.control') }}</th>
        </tr>
        </thead>
        <tbody>
          @unless ($languages->count())
            <tr>
            <td colspan="5">{{ __('admin.no_data') }}</td>
            </tr>
          @else
          @foreach($languages as $language)
            <tr>
              <td>{!! $language->name !!}</td>
              <td>{!! $language->code !!}</td>
              <td>{!! $language->dir !!}</td>
              <td>
                <img src="{{ $language->photo_path() }}" class="img img-responsive" width="50px">
              </td>
              <td>
                <a href="{{ route('languages.edit', $language) }}" class="btn btn-info btn-sm btn-flat"><i class="icon ion-ios-compose-outline"></i></a>
              </td>
            </tr>
          @endforeach
          @endunless
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
@stop
