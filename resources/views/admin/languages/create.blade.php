@extends('admin.layouts.app')

@section('title', __('languages.create_new_language'))

@section('pageheader')
  <h1>
    {{ __('languages.languages') }}
  <small>{{ __('languages.create_new_language') }}</small>
  </h1>
@stop

@section('content')
  <div class="row">
  {!! Form::open(['route' => 'languages.store', 'files' => true]) !!}
    <!-- left column -->
    <div class="col-md-7">

      <div class="box">
        <div class="box-header with-border">
        <h3 class="box-title">{{ __('languages.create_new_language') }}</h3>
        </div>

          <div class="box-body">
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name">{{ __('languages.name') }}</label>
              {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => __('languages.name')]) !!}

              @if ($errors->has('name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
            </div>

            <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
            <label for="code">{{ __('languages.code') }}</label>
              {!! Form::text('code', null, ['class' => 'form-control', 'placeholder' => __('languages.code')]) !!}

              @if ($errors->has('code'))
                  <span class="help-block">
                      <strong>{{ $errors->first('code') }}</strong>
                  </span>
              @endif
            </div>

            <div class="form-group">
            <label for="dir">{{ __('languages.direction') }}</label>
              {{ Form::select('dir', ['ltr' => 'LTR', 'rtl' => 'RTL'], 'ltr', ['class' => 'form-control selectpicker']) }}
            </div>


            <div class="form-group">
            <label for="image">{{ __('languages.flag') }}</label>
                <file-field></file-field>
            </div>
          </div>

          <div class="box-footer">
            <div class="col-md-6">
            <button class="btn btn-info btn-flat btn-block m-b-10"><i class="icon ion-ios-plus-outline"></i>&nbsp;{{ __('admin.save') }}</button>
            </div>
            <div class="col-md-6">
            <a href="{{ route('languages.index') }}" class="btn btn-default btn-flat btn-block"><i class="icon ion-log-out"></i>&nbsp;{{ __('admin.back') }}</a>
            </div>
          </div>
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
  {!! Form::close() !!}
  </div>
@stop
