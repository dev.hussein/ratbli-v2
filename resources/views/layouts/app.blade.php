<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('partials._head')
</head>
<body>
    <div id="app">
        @include('partials._nav')

        @yield('content')
    </div>
    @include('partials._javascript')
</body>
</html>