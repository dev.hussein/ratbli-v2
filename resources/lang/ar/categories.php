<?php

return [
    'categories' => 'الأقسام',
    'name' => 'الاسم',
    'image' => 'الصورة',
    'create_new_category' => 'إضافة قسم',
    'edit_category' => 'تعديل بيانات قسم',
    'main_category' => 'القسم الرئيسى',
    'parent' => 'رئيسى',
    'service' => 'الخدمة'
];