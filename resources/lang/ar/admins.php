<?php

return [
    'admins'   => 'المشرفين',
    'name' => 'الاسم',
    'username' => 'اسم المستخدم',
    'email' => 'البريد الالكترونى',
    'password' => 'كلمة المرور',
    'permissions' => 'الصلاحيات',
    'create_new_admin' => 'إضافة مشرف جديد',
    'edit_admin' => 'تعديل بيانات مشرف',
    'change_password' => 'تغيير كلمة المرور',
    'new_password' => 'كلمة المرور الجديدة'
];