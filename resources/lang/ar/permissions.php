<?php

return [
    'permissions' => 'الصلاحيات',
    'name' => 'الاسم',
    'create_new_permission' => 'إضافة صلاحية',
    'edit_permission' => 'تعديل بيانات صلاحية',
];