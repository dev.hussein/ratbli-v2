<?php

return [
    'users'   => 'المستخدمين',
    'name' => 'الاسم',
    'mobile' => 'رقم الجوال',
    'email' => 'البريد الالكترونى',
    'password' => 'كلمة المرور',
    'create_new_user' => 'إضافة مستخدم جديد',
    'edit_user' => 'تعديل بيانات مستخدم',
    'change_password' => 'تغيير كلمة المرور',
    'new_password' => 'كلمة المرور الجديدة'
];