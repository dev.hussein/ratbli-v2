<?php

return [
    'home'   => 'الرئيسية',
    'admins' => 'المشرفين',
    'users' => 'المستخدمين',
    'permissions' => 'الصلاحيات',
    'languages' => 'اللغات',
    'services' => 'الخدمات',
    'categories' => 'الأقسام',
];