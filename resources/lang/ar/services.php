<?php

return [
    'services' => 'الخدمات',
    'name' => 'الاسم',
    'image' => 'الصورة',
    'create_new_service' => 'إضافة خدمة',
    'edit_service' => 'تعديل بيانات خدمة'
];