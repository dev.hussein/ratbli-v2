<?php

return [
    'languages' => 'اللغات',
    'name' => 'الاسم',
    'code' => 'الكود',
    'direction' => 'اتجاه التصميم',
    'flag' => 'العلم',
    'edit_language' => 'تعديل بيانات لغة',
    'create_new_language' => 'إضافة لغة'
];