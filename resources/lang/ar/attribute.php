<?php

return [

	'attribute'			=> 'التصنيف',
	'attributes'		=> 'التصنيفات',
	'attribute_set'		=> 'مجموعة التصنيف',
	'attribute_sets'	=> 'مجموعات التصنيف',
	'name'				=> 'الاسم',
	'type'				=> 'النوع',
	'text'				=> 'نص',
	'textarea'			=> 'نص مقال',
	'date'				=> 'تاريخ',
	'multiple_select'	=> 'الاختيار متعدد',
	'dropdown'			=> 'الاختيار',
	'media'				=> 'ميديا',
	'add_option'		=> 'اضافة اختيار',
	'option'			=> 'اختيار',
	'default'			=> 'افتراضى',
];