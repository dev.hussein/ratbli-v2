<?php

return [
    'home'   => 'الرئيسية',
    'statistics' => 'الاحصائيات',
    'time' => 'الوقت',
    'date' => 'التاريخ',
    'show_all' => 'عرض الكل',
    'create_new' => 'إضافة جديد',
    'refresh' => 'تحديث',
    'status' => 'الحالة',
    'control' => 'التحكم',
    'save' => 'حفظ',
    'back' => 'عودة',
    'active' => 'مفعل',
    'disabled' => 'معطل',
    'no_data' => 'لا توجد بيانات',
    'menu' => 'القائمة',
    'admin' => 'لوحة',
    'panel' => 'المعلومات'
];