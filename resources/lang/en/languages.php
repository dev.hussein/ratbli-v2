<?php

return [
    'languages' => 'Languages',
    'name' => 'Name',
    'code' => 'Code',
    'direction' => 'Direction',
    'flag' => 'Flag',
    'edit_language' => 'Edit language',
    'create_new_language' => 'Create new language'
];