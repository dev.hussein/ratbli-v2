<?php

return [
    'admins'   => 'Admins',
    'name' => 'Name',
    'username' => 'Username',
    'email' => 'Email',
    'password' => 'Password',
    'permissions' => 'Permissions',
    'create_new_admin' => 'Create new admin',
    'edit_admin' => 'Edit admin',
    'change_password' => 'Change password',
    'new_password' => 'New password'
]; 