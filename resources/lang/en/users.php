<?php

return [
    'users'   => 'Users',
    'name' => 'Name',
    'mobile' => 'Mobile',
    'email' => 'Email',
    'password' => 'Password',
    'create_new_user' => 'Add new user',
    'edit_user' => 'Edit user',
    'change_password' => 'Change password',
    'new_password' => 'New password'
];