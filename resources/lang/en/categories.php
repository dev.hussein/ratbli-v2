<?php

return [
    'categories' => 'Categories',
    'name' => 'Name',
    'image' => 'Image',
    'create_new_category' => 'Create new category',
    'edit_category' => 'Edit category',
    'main_category' => 'Main category',
    'parent' => 'Parent',
    'service' => 'Service'
];