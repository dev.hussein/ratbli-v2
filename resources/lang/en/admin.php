<?php

return [
    'home'   => 'Home',
    'statistics' => 'Statistics',
    'time' => 'Time',
    'date' => 'Date',
    'show_all' => 'List all',
    'create_new' => 'Create new',
    'refresh' => 'Refresh',
    'status' => 'Status',
    'control' => 'Control',
    'save' => 'Save',
    'back' => 'Back',
    'active' => 'Active',
    'disabled' => 'Disabled',
    'no_data' => "There's no data available",
    'menu' => 'Menu',
    'admin' => 'Admin',
    'panel' => 'Panel'
];