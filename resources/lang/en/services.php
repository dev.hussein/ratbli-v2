<?php

return [
    'services' => 'Services',
    'name' => 'Name',
    'image' => 'Image',
    'create_new_service' => 'Create new service',
    'edit_service' => 'Edit service'
];