<?php

return [
    'home'   => 'Home',
    'admins' => 'Admins',
    'users' => 'Users',
    'permissions' => 'Permissions',
    'languages' => 'Languages',
    'services' => 'Services',
    'categories' => 'Categories',
];