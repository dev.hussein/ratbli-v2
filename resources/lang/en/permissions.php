<?php

return [
    'permissions' => 'Permissions',
    'name' => 'Name',
    'create_new_permission' => 'Create new permission',
    'edit_permission' => 'Edit permission',
];