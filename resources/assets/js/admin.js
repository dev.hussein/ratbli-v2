window.Vue = require('vue');

window.axios = require('axios');

window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window.App.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};

window.events = new Vue();

import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyB0T-uGFTd8aQ_a7mZmhN0hX9F5dhVUeH4',
    libraries: 'places',
  }
})

import SidebarCollapse from "./components/backend/SidebarCollapse";
import Clock from "./components/backend/Clock";
import Date from "./components/backend/Date";
import ImageUpload from "./components/backend/ImageUpload";
import ConfirmModal from "./components/backend/ConfirmModal";
import FileField from "./components/backend/FileField";
import GoogleMap from "./components/backend/GoogleMap";
import FileMultiplePreview from "./components/backend/FileMultiplePreview";

Vue.component('sidebar-collapse', SidebarCollapse)
Vue.component('clock', Clock)
Vue.component('date', Date)
Vue.component('image-upload', ImageUpload)
Vue.component('confirm-modal', ConfirmModal)
Vue.component('file-field', FileField)
Vue.component('google-map', GoogleMap)
Vue.component('file-multiple-preview', FileMultiplePreview)

const app = new Vue({
  el: '#app',
  data: {
    message: 'Hello World!'
  }
})
