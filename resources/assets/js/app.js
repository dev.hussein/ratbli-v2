//require('./bootstrap');
window.Vue = require('vue');

window.axios = require('axios');

window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window.App.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};

//Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('country-form', require('./components/frontend/CountryForm.vue'));

const app = new Vue({
    el: '#app'
});
