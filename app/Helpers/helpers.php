<?php

if (! function_exists('uploadImage')) {
	function uploadImage($file, $path = '') 
	{
		return app('App\Http\Controllers\ImageController')->uploadImage($file, $path);
	}
}
//===========================================================================
if (! function_exists('createImageFromBase64')) {
	function createImageFromBase64($data, $path = '')
	{
		return app('App\Http\Controllers\ImageController')->createImageFromBase64($data, $path);
	}
}
//===========================================================================
if (! function_exists('getThumbnail') ) {
	function getThumbnail($img_path, $width, $height, $type = "fit")
	{
	    return app('App\Http\Controllers\ImageController')->getImageThumbnail($img_path, $width, $height, $type);
	}
}
//===========================================================================
if (! function_exists('commonmark')) {
    function commonmark($commonmark)
    {
        return (new League\CommonMark\CommonMarkConverter)->convertToHtml($commonmark);
    }
}
//===========================================================================
if (! function_exists('limit_words')) {
    function limit_words($string, $words = 100, $end = '...')
    {
        return \Illuminate\Support\Str::words($string, $words, $end);
    }
}
//===========================================================================
if (! function_exists('humanize_date')) {
    function humanize_date(Carbon\Carbon $date, $format = 'd F Y, H:i'): string
    {
        return $date->format($format);
    }
}
//===========================================================================
if (! function_exists('respond')) {
	function respond($data, $msg = '', $headers = [])
	{
		return app('App\Http\Controllers\Api\ApiController')->respond($data, $msg, $headers);
	}
}
//===========================================================================
if (! function_exists('respondWithError')) {
	function respondWithError($error, $errorMessages = [])
	{
		return app('App\Http\Controllers\Api\ApiController')->respondWithError($error, $errorMessages);
	}
}
//===========================================================================
if (! function_exists('fcm')) {
	function fcm($registration_ids, $notification, $data = [])
	{
		return app('App\Http\Controllers\Api\ApiController')->fcm($registration_ids, $notification, $data);
	}
}
//===========================================================================
if (! function_exists('calculateDistance')) {
	function calculateDistance($lat1, $lng1, $lat2, $lng2, $unit = "K") 
	{
		return app('App\Http\Controllers\Api\ApiController')->calculateDistance($lat1, $lng1, $lat2, $lng2, $unit);
	}
}
//===========================================================================
if (! function_exists('is_open')) {
	function is_open($working_days, $start_time, $end_time) 
	{
		return app('App\Http\Controllers\Api\ApiController')->is_open($working_days, $start_time, $end_time);
	}
}
//===========================================================================
if (! function_exists('languages')) {
	function languages() 
	{
		$languages = App\Language::all();

		return $languages;
	}
}
//===========================================================================