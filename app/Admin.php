<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\AdminResetPasswordNotification;
use Spatie\Permission\Traits\HasRoles;
use OwenIt\Auditing\Contracts\Auditable;

class Admin extends Authenticatable implements Auditable
{
    use HasApiTokens, Notifiable, HasRoles, \OwenIt\Auditing\Auditable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $guard_name = 'admin';
    
    protected $guard = 'admin';

    protected $fillable = [
        'name', 'username', 'email', 'avatar', 'password', 'ip_address', 'active', 'options',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'last_login_at'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function avatar_path()
    {
        if ($this->avatar != null && $this->avatar != '') {
            return url($this->avatar);
        }
        else {
            return url('assets/dashboard/img/avatar.png');
        }
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPasswordNotification($token));
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS and MUTATORS
    |--------------------------------------------------------------------------
    */
    public function getOptionsAttribute($value)
    {
        return unserialize($value);
    }
    
    public function setOptionsAttribute($value)
    {
        $this->attributes['options'] = serialize($value);
    }

}
