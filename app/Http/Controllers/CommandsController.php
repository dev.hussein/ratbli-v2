<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;

class CommandsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
    	$tables_in_db = DB::select('SHOW TABLES');
    	$db = "Tables_in_".env('DB_DATABASE');
        $tables = [];
        foreach($tables_in_db as $table){
            $tables[] = $table->{$db};
        }

    	return view('admin.commands.index', compact('tables'));
    }

    public function runMigration()
    {
    	Artisan::call('migrate');
    	return back();
    }

    public function installDatabase()
    {
    	Artisan::call('migrate:fresh');
    	Artisan::call('db:seed');
    	return back();
    }

    public function installPassport()
    {
    	Artisan::call('migrate', ['--path' => 'vendor/laravel/passport/database/migrations']);
    	return back();
    }

    public function optimize()
    {
        Artisan::call('config:cache');
        Artisan::call('route:cache');
        return back();
    }

    public function clearCache()
    {
    	Artisan::call('config:clear');
        Artisan::call('route:cache');
        Artisan::call('view:clear');
    	return back();
    }

    public function tableTruncate($table)
    {
    	DB::table($table)->truncate();
    	return back();
    }

    public function tableDelete($table)
    {
    	DB::statement("DROP TABLE $table");
    	return back();
    }

    public function tableShow($table)
    {
    	$data = DB::table($table)->get();
    	return $data;
    }

}
