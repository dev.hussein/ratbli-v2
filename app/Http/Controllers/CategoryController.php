<?php

namespace App\Http\Controllers;

use App\Category;
use App\CategoryTranslation;
use App\ServiceTranslation;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::where('parent_id', '=', 0)->get();

        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = CategoryTranslation::whereHas('category', function($q) {
                                                $q->where('parent_id', '=', 0);
                                           })->where('locale', app()->getLocale())->pluck('name','category_id')->all();

        $services = ServiceTranslation::where('locale', app()->getLocale())->pluck('name','service_id')->all();

        return view('admin.categories.create', compact('categories', 'services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            //'name' => 'required|max:100|unique:categories',
            'service_id' => 'required',
            'parent_id' => 'required'
        ]);

        $category = Category::create([
                        //'name' => request('name'),
                        'parent_id' => request('parent_id'),
                        'service_id' => request('service_id'),
                        'status' => request('status'),
                        'image' => request()->file('image') ? uploadImage(request()->file('image'), 'categories') : ''
                    ]);

        foreach(languages() as $lang) 
        {
            $category->translateOrNew($lang->code)->name = request("name_".$lang->code);
        }

        $category->save();

        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categories = CategoryTranslation::whereHas('category', function($q) use ($category) {
                                                $q->where([
                                                    ['parent_id', '=', 0],
                                                    ['id', '<>', $category->id]
                                                ]);
                                           })->where('locale', app()->getLocale())->pluck('name','category_id')->all();

        $services = ServiceTranslation::where('locale', app()->getLocale())->pluck('name','service_id')->all();

        return view('admin.categories.edit', compact('categories', 'category', 'services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        request()->validate([
            //'name' => ['required', 'max:100', Rule::unique('categories')->ignore($category->id)],
            'service_id' => 'required',
            'parent_id' => 'required'
        ]);

        //$category->name = request('name');
        $category->service_id = request('service_id');
        $category->parent_id = request('parent_id');
        $category->status = request('status');

        if(request()->file('image')) {
            \File::delete($category->image);
            $category->image = uploadImage(request()->file('image'), 'categories');
        }

        $category->update();

        foreach(languages() as $lang) 
        {
            $category->translateOrNew($lang->code)->name = request("name_".$lang->code);
        }

        $category->save();

        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        \File::delete($category->image);
        
        $category->delete();

        return response()->json([
            'success' => true,
            'message' => 'تم الحذف بنجاح.'
        ]);
    }
}
