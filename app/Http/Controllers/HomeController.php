<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['authorize', 'auth']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');

        //$categories = \App\Category::where('parent_id', '=', 0)->get();

        //$allCategories = \App\Category::pluck('name','id')->all();

        //return view('tree.parent',compact('categories','allCategories'));

        /*foreach($categories as $category) {

                    echo "<p>".$category->name."</p>";

                    $this->subcategories($category);
                    
        }*/


    }

    public function subcategories($category) 
    {
        if(count($category->childs)) {
            foreach($category->childs as $child) {
                echo "<p>--".$child->name."</p>";
            }
            if(count($child->childs)) {
                $this->subcategories($child);
            }

            //@include('manageChild',['childs' => $child->childs])

            //@endif   
        }
    }

}
