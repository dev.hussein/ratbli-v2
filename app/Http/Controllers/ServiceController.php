<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ServiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all();

        return view('admin.services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            //'name' => 'required|max:100|unique:services',
            'status' => 'required',
            //'image' => 'required|image'
        ]);

        $service = Service::create([
                        'status' => request('status'),
                        'image' => request()->file('image') ? uploadImage(request()->file('image'), 'services') : ''
                    ]);

        foreach(languages() as $lang) 
        {
            $service->translateOrNew($lang->code)->name = request("name_".$lang->code);
        }

        $service->save();

        flash('تم الأضافة بنجاح!')->success();

        return redirect()->route('services.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        return view("admin.services.edit", compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        request()->validate([
            //'name' => ['required', 'max:100', Rule::unique('services')->ignore($service->id)],
            'status' => 'required'
        ]);

        $service->status = request('status');

        if(request()->file('image')) {
            \File::delete($service->flag);
            $service->flag = uploadImage(request()->file('image'), 'services');
        }

        $service->update();

        foreach(languages() as $lang) 
        {
            $service->translateOrNew($lang->code)->name = request("name_".$lang->code);
        }

        $service->save();

        flash('تم التعديل بنجاح!')->success();

        return redirect()->route('services.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        //
    }
}
