<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class RegisterConfirmationController extends Controller
{
    /**
     * Confirm a user's email address.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index()
    {
        $user = User::where('confirmation_token', request('token'))->first();

        if (! $user) {
            flash('Unknown token.')->error();
            return redirect(route('login'));
        }

        $user->confirm();

        flash('Your account is now confirmed!')->success();
        return redirect(route('login'));
    }
}
