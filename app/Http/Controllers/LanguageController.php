<?php

namespace App\Http\Controllers;

use App\Language;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class LanguageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => ['swap']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$languages = Language::all();

        return view('admin.languages.index', compact('languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.languages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required|max:100|unique:languages',
            'code' => 'required|min:2|unique:languages',
            'dir' => 'required',
            //'image' => 'required|image'
        ]);

        Language::create([
            'name' => request('name'),
            'code' => request('code'),
            'dir' => request('dir'),
            'flag' => request()->file('image') ? uploadImage(request()->file('image')) : ''
        ]);

        flash('تم الأضافة بنجاح!')->success();

        return redirect()->route('languages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function show(Language $language)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function edit(Language $language)
    {
        return view("admin.languages.edit", compact('language'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Language $language)
    {
        request()->validate([
            'name' => ['required', 'max:100', Rule::unique('languages')->ignore($language->id)],
            'code' => ['required', 'min:2', Rule::unique('languages')->ignore($language->id)],
            'dir' => 'required'
        ]);

        $language->name = request('name');
        $language->code = request('code');
        $language->dir = request('dir');

        if(request()->file('image')) {
            \File::delete($language->flag);
            $language->flag = uploadImage(request()->file('image'));
        }

        $language->update();

        flash('تم التعديل بنجاح!')->success();

        return redirect()->route('languages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        //
    }

    public function swap($lang)
    {
        session()->put('locale', $lang);

        return redirect()->back();
    }
}
