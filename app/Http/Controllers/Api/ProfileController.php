<?php

namespace App\Http\Controllers\API;

use App\User;
use App\Http\Resources\User as UserResource;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
    	return new UserResource(Auth::user());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Validator::make($request->all(), [
            'mobile' => [
                'required',
                'regex:/^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/',
                Rule::unique('users')->ignore(auth()->user()->id)
            ],
            'name' => 'required|string|max:50',
            'email' => [
            	'sometimes', 'nullable', 'string', 'email', 'max:191',
            	Rule::unique('users')->ignore(auth()->user()->id)
            ]
        ])->validate();

        $user = Auth::user();

        $user->mobile = request('mobile');
        $user->name = request('name');
        $user->email = request('email');

        if(request('avatar')) $user->avatar = createImageFromBase64(request('avatar'), 'profiles');

        $user->update();

        return (new UserResource($user))
            ->response()
            ->setStatusCode(202);
    }
}
