<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Http\Resources\Category as CategoryResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $locale)
    {
    	app()->setLocale($locale);

        $categories = Category::withTranslation()->where('status', 1)->get();

        return CategoryResource::collection($categories); 
    }
}
