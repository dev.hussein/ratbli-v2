<?php

namespace App\Http\Controllers\API;

use App\Language;
use App\Http\Resources\Language as LanguageResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return LanguageResource::collection(Language::all());
    }
}
