<?php

namespace App\Http\Controllers\API;

use App\User;
use App\Http\Resources\User as UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'mobile' => [
                'required',
                'regex:/^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/',
                'unique:users'
            ],
            'name' => 'required|string|max:50',
            'email' => 'sometimes|nullable|string|email|max:191|unique:users',
            'password' => 'required|string|min:6|confirmed'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::forceCreate([
            'name' => $data['name'],
            'email' => $data['email'],
            'mobile' => $data['mobile'],
            'password' => bcrypt($data['password']),
            'lat' => $data['lat'],
	        'lng' => $data['lng'],
            'confirmation_token' => mt_rand(1000, 9999)
        ]);

        return $user;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request, $locale)
    {
    	app()->setLocale($locale);

    	$this->validator($request->all())->validate();

    	$user = $this->create($request->all());

        $data = [
        	'token'  => $user->createToken('MyApp')->accessToken,
        	'mobile' => $user->mobile,
        	'confirmed' => false
        ];

        //send sms with confirmation code;

        return response()->json($data, 201);
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request, $locale)
    {
    	app()->setLocale($locale);

    	Validator::make($request->all(), [
            'mobile'   => 'required',
            'password' => 'required'
        ])->validate();

    	if(Auth::attempt(['mobile' => request('mobile'), 'password' => request('password'), 'status' => 1]))
		{
			$user = Auth::user();

			if($user->confirmed === 1)
			{
				return new UserResource($user);
			}
			else
			{
				$data = [
                    'errors' => ['msg' => [__('auth.confirm')]]
				];

				return response()->json($data, 422);
			}
    	}
    	else
    	{
    		$data = [
    			'errors' => ['msg' => [__('auth.failed')]]
    		];

    		return response()->json($data, 422);
    	}
    }

    /**
     * Confirm the user account.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function confirm(Request $request, $locale, $mobile)
    {
    	app()->setLocale($locale);

    	Validator::make($request->all(), [
            'confirmation_token' => 'required|integer',
        ])->validate();

        $user = User::where('mobile', $mobile)->first();

        if($user->confirmation_token === request('confirmation_token'))
        {
        	$user->confirm();

        	return new UserResource($user);
        }
        else
        {
    		$data = [
    			'errors' => ['msg' => [__('auth.confirmation_failed')]]
    		];

    		return response()->json($data, 422);
        }
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
    	if (Auth::check()) Auth::user()->AauthAcessToken()->delete();

		$response = 'You have been succesfully logged out!';

    	return response()->json($response, 200);
    }
}
