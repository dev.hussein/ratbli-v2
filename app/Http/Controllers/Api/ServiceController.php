<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Service;
use App\Http\Resources\Category as CategoryResource;
use App\Http\Resources\Service as ServiceResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $locale)
    {
    	app()->setLocale($locale);

        $services = Service::withTranslation()->where('status', 1)->get();

        return ServiceResource::collection($services); 
    }

    /**
     * Display list of Categories filtered by service.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCategories(Request $request, $locale, $id)
    {
        app()->setLocale($locale);

        $service = Service::findOrFail($id);

        $categories = $service->categories()->withTranslation()->where('status', 1)->get();

        return CategoryResource::collection($categories);
    }
}
