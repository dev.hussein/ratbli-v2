<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    protected $statusCode = Response::HTTP_OK;

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function respond($data, $msg = '', $headers = [])
    {
    	$response = [
            'status' => true,
            'data'   => $data,
            'msg'    => $msg,
        ];

        return response()->json($response, $this->getStatusCode(), $headers);
    }

    public function respondWithError($error, $errorMessages = [])
    {
    	$response = [
            'status' => false,
            'data'   => $errorMessages,
            'msg'    => $error,
        ];

        return response()->json($response, $this->getStatusCode());
    }

    public function fcm($registration_ids, $notification, $data = [])
    {
    	$fields = [
    		'registration_ids' => $registration_ids,
    		'notification'     => $notification,
    		'data'             => $data
    	];

    	$headers = [
    		'Authorization: key=' . env('API_ACCESS_KEY', 'null'),
			'Content-Type:  application/json'
    	];

		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );

    }

    public function calculateDistance($lat1, $lng1, $lat2, $lng2, $unit = "K") 
    {
    	$theta = $lng1 - $lng2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);

		if ($unit == "K") {
			$km = ($miles * 1.609344);
			return round($km, 1);
		} else if ($unit == "N") {
			return ($miles * 0.8684);
		} else {
			return $miles;
		}
    }

    public function is_open($working_days, $start_time, $end_time)
    {
		$result = false;

		$days = unserialize($working_days);

		if (in_array(date('N'), $days)   ) 
		{
			if (time() >= strtotime($start_time) and time() <= strtotime($end_time))
			{
				$result = true;
			}
			else
			{
				$result = false;
			}
		}

		return $result;
    }
}
