<?php

namespace App\Http\Controllers\Api;

use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function postSidebarToggle(Request $request)
    {
    	$adminId = auth()->user('admin')->id;
    	$admin = Admin::where('id', $adminId)->first();

    	$options = $admin->options;

    	if (isset($options['sidebar'])) {
            $options['sidebar'] = !$options['sidebar'];
        }

    	$admin->options = $options;
    	$admin->save();

    	return response()->json(['data' => $admin], 200);
    }

    public function postUploadAvatar(Request $request)
    {
        $path = createImageFromBase64(request('img'));

        $admin = Admin::find(request('id'));

        if ($admin->avatar != '') {
            \File::delete($admin->avatar);
        }

        $admin->avatar = $path;
        $admin->save();

        return response()->json(['data' => url($path)], 201);

    }
}
