<?php

namespace App\Http\Controllers;

use App\Admin;
use DataTables;
use App\Http\Requests\Admins\StoreAdminRequest;
use App\Http\Requests\Admins\UpdateAdminRequest;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = Admin::all();

        if (request()->expectsJson()) {
            return DataTables::of($admins)->make(true);
        }

        return view('admin.admins.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::all();

        return view('admin.admins.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAdminRequest $request)
    {
        $admin = Admin::create([
          'name' => request('name'),
          'email' => request('email'),
          'username' => request('username'),
          'password' => bcrypt(request('password')),
          'active' => request('active'),
          'ip_address' => request()->ip()
        ]);

        $admin->syncPermissions(request('permission_id'));

        flash('تم إضافة المستخدم بنجاح!')->success();

        return redirect()->route('admins.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        $permissions = Permission::all();

        $current_permissions = $admin->permissions()->pluck('permission_id')->toArray();

        return view("admin.admins.edit", compact('admin', 'permissions', 'current_permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAdminRequest $request, Admin $admin)
    {
        $admin->name = $request->name;
        $admin->username = $request->username;
        $admin->email = $request->email;
        $admin->active = $request->active;

        if ($request->password_change) {
          $admin->password = bcrypt($request->password);
        }

        $admin->update();

        $admin->syncPermissions(request('permission_id'));

        flash('تم تعديل المستخدم بنجاح!')->success();

        return redirect()->route('admins.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        $admin->delete();

        return response()->json([
            'success' => true,
            'message' => 'تم الحذف بنجاح.'
        ]);
    }

    public function profile()
    {
        $admin = auth()->user('admin');

        return view("admin.admins.profile", compact('admin'));
    }

    public function updateProfile(UpdateAdminRequest $request, Admin $admin)
    {
        $admin->name = $request->name;
        $admin->active = $request->active;

        if ($request->password_change) {
          $admin->password = bcrypt($request->password);
        }

        $admin->update();

        flash('تم تعديل البيانات بنجاح!')->success();

        return back();
    }
}
