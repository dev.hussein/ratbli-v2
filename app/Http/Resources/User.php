<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'     => $this->id,
            'name'   => $this->name,
            'email'  => $this->email,
            'mobile' => $this->mobile,
            'lat'    => $this->lat,
            'lng'    => $this->lng,
            'avatar' => url($this->avatar),
            'token'  => $this->createToken('MyApp')->accessToken
        ];
    }
}
