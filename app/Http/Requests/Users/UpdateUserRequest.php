<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile' => [
                'required',
                'regex:/^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/',
                Rule::unique('users')->ignore($this->user->id)
            ],
            'name' => 'required|string|max:50',
            'email' => [
                'sometimes', 'nullable', 'string', 'email', 'max:191',
                Rule::unique('users')->ignore($this->user->id)
            ]
        ];
    }
}
