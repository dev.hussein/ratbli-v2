<?php

namespace App\Http\Requests\Admins;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('admins')->ignore($this->admin->id)],
            'username' => ['required', 'string', 'max:20', Rule::unique('admins')->ignore($this->admin->id)],
            'password' => 'required_with:password_change',
            'active' => 'required'
        ];
    }
}
