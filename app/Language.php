<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $fillable = ['name', 'code', 'dir', 'flag'];

    public $timestamps = false;

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function photo_path()
    {
        if ($this->flag != null && $this->flag != '') {
            return url($this->flag);
        }
        else {
            return url('assets/dashboard/img/default.png');
        }
    }
}
