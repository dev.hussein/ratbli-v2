<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use \Dimsav\Translatable\Translatable;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    public $translatedAttributes = ['name'];
    
    protected $fillable = ['image', 'parent_id', 'service_id', 'sort', 'status'];

    public $timestamps = false;

    /*
    |--------------------------------------------------------------------------
    | EVENTS
    |--------------------------------------------------------------------------
    */
    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($category) {
            foreach ($category->childs as $value) {
                \File::delete($value->photo);
            }
            $category->childs->each->delete();
        });
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function photo_path()
    {
        if ($this->image != null && $this->image != '') {
            return url($this->image);
        }
        else {
            return url('assets/dashboard/img/default.png');
        }
    }

    public function check_status()
    {
        return ($this->status == 1) ? '<i class="fa fa-lg fa-toggle-on text-success"></i>' : '<i class="fa fa-lg fa-toggle-off text-danger"></i>';
    } 

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function childs()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function service()
    {
        return $this->belongsTo('App\Service');
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS and MUTATORS
    |--------------------------------------------------------------------------
    */
    /*public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }*/

}
