<?php

namespace App;

use App\Translation;
use Illuminate\Support\Facades\App;

trait Translatable 
{
    public function translations()
    {
        return $this->morphMany(Translation::class, 'translatable');
    }

    public function getTranslationAttribute()
    {
        return $this->translations->firstWhere('language', App::getLocale());
    }
}
