<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use \Dimsav\Translatable\Translatable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    public $translatedAttributes = ['name'];

    protected $fillable = ['status', 'image'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function photo_path()
    {
        if ($this->image != null && $this->image != '') {
            return url($this->image);
        }
        else {
            return url('assets/dashboard/img/default.png');
        }
    } 

    public function check_status()
    {
    	return ($this->status == 1) ? '<i class="fa fa-lg fa-toggle-on text-success"></i>' : '<i class="fa fa-lg fa-toggle-off text-danger"></i>';
    }  

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function categories()
    {
        return $this->hasMany('App\Category');
    }
}
