<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    protected $casts = [
        'content' => 'array',
    ];

    protected $fillable = [
        'content',
        'language',
    ];

    public function translatable()
    {
        return $this->morphTo();
    }
}
