<?php

use App\Admin;
use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('admins')->truncate();

        Admin::create([
          'name' => 'Admin',
          'username' => 'admin',
          'email' => 'admin@app.com',
          'password' => bcrypt('123456'),
          'ip_address' => request()->ip(),
          'active' => 1,
          'remember_token' => str_random(10)
        ]);
    }
}
