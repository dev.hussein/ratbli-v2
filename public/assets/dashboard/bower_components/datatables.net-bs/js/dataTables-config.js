$(function () {
  $('#dataTable').DataTable({
  	'info' : false,
    language: {
      searchPlaceholder: 'بحث...',
      sSearch: '',
      lengthMenu: '_MENU_',
    }
  })
})