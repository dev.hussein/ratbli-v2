let table = $('#dt').DataTable({
    'info' : false,
    language: {
      searchPlaceholder: '...',
      sSearch: '',
      lengthMenu: '_MENU_',
    },
	pageLength: 10,
    processing: true,
    serverSide: true,
    ajax: {
    	url: url
    },
    columns: [
        { data: 'name' },
        { data: 'email'},
        { data: 'mobile'},
        { data: 'status'}
    ],
	  "columnDefs": [ {
	    "targets": 3,
	    "data": "action",
	    "render": function ( data, type, row, meta ) {
	      return check_status(row.status);
	    }
	  },
	  {
	    "targets": 4,
	    "data": "action",
	    "render": function ( data, type, row, meta ) {
	      return '<a href="'+url+'/'+row.id+'/edit'+'" class="btn btn-info btn-flat btn-sm"><i class="icon ion-ios-compose-outline"></i></a>';
	    }
	  }]
});

function reload_table()
{
  table.ajax.reload(null, false);  
}

function check_status(status)
{
	if(status == 1) {
		return '<i class="fa fa-lg fa-toggle-on text-success"></i>';
	} else {
		return '<i class="fa fa-lg fa-toggle-off text-danger"></i>';
	}
}
